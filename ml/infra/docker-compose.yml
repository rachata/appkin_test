version: '3'

networks:
  wgo-network:
    driver: bridge

services:
  app:
    build:
      context: .
      dockerfile: ./rest-api/Dockerfile
    ports:
      - "8383:8383"
    restart: on-failure
    volumes:
      - ./rest-api:/app/rest-api
      - ./public/predict:/app/public/predict
    networks:
      - wgo-network



  kong-database:
    image: postgres:latest
    container_name: kong-database
    restart: always
    ports:
      - ${DB_PORT}:5432
    networks:
      - wgo-network
    volumes:
      - db:/var/lib/postgresql/data
    environment:
      - POSTGRES_DB=${DB_NAME}
      - POSTGRES_USER=${DB_USER}
      - POSTGRES_PASSWORD=${DB_PASS}

  kong-migrations:
    image: kong:latest
    networks:
      - wgo-network
    depends_on:
      - kong-database
    environment:
      - KONG_DATABASE=postgres
      - KONG_PG_HOST=kong-database
      - KONG_PG_PASSWORD=${DB_PASS}
      - KONG_CASSANDRA_CONTACT_POINTS=kong-database
    command: kong migrations bootstrap
    restart: on-failure

  kong:
    image: kong:latest
    container_name: kong
    restart: always
    ports:
      - ${API_PORT}:8000
      - ${APIS_PORT}:8443
      - ${LISTENER_PORT}:8001
      - ${LISTENERS_PORT}:8444
    links:
      - kong-database:kong-database
    networks:
      - wgo-network
    depends_on:
      - kong-migrations
    environment:
      - LC_CTYPE=en_US.UTF-8
      - LC_ALL=en_US.UTF-8
      - KONG_DATABASE=postgres
      - KONG_PG_HOST=kong-database
      - KONG_PG_USER=${DB_USER}
      - KONG_PG_PASSWORD=${DB_PASS}
      - KONG_CASSANDRA_CONTACT_POINTS=kong-database
      - KONG_PROXY_ACCESS_LOG=/dev/stdout
      - KONG_ADMIN_ACCESS_LOG=/dev/stdout
      - KONG_PROXY_ERROR_LOG=/dev/stderr
      - KONG_ADMIN_ERROR_LOG=/dev/stderr
      - KONG_ADMIN_LISTEN=0.0.0.0:${LISTENER_PORT}, 0.0.0.0:${LISTENERS_PORT} ssl
    
  konga:
    platform: linux/amd64
    image: pantsel/konga:latest
    container_name: kong-konga
    restart: always
    ports:
      - ${APP_PORT}:1337
    networks:
      - wgo-network
    volumes:
      - data:/app/kongadata
    links:
      - kong:kong
    environment:
      - NODE_ENV=production

  grafana:
    image: grafana/grafana:latest
    restart: always
    container_name: grafana
    ports:
      - ${GRAFANA_PORT}:${GRAFANA_PORT}

    external_links:
      - prometheus
    environment:
      GF_INSTALL_PLUGINS: grafana-clock-panel, grafana-simple-json-datasource
    networks:
      - wgo-network

  prometheus:
    image: prom/prometheus:latest
    container_name: prometheus
    volumes:
      - ./prometheus.yml:/etc/prometheus/prometheus.yml
      - prometheus_data:/promtheus
    command:
     - '--config.file=/etc/prometheus/prometheus.yml'
    ports:
      - "9090:9090"
    expose:
      - "9090"
    restart: always
    networks:
      - wgo-network

  node_exporter:
    image: prom/node-exporter:latest
    container_name: node_exporter
    ports:
      - "9100:9100"
    expose:
      - "9100"
    restart: always
    networks:
      - wgo-network

  rabbitmq:
    image: "rabbitmq:3-management"
    container_name: rabbitmq
    restart: always
    ports:
      - "5672:5672" 
      - "15672:15672" 
    networks:
      - wgo-network
    environment:
      RABBITMQ_DEFAULT_USER: ${RABBITMQ_DEFAULT_USER}
      RABBITMQ_DEFAULT_PASS: ${RABBITMQ_DEFAULT_PASS}
      RABBITMQ_ERLANG_COOKIE: "wgo"  # Change this to a secure value


  geoserver:
    platform: linux/amd64
    image: kartoza/geoserver
    container_name: geoserver
    volumes:
      - geoserver-data:/opt/geoserver/data_dir
    ports:
      - 8080:8080
    networks:
      - wgo-network
    environment:
      - GEOSERVER_ADMIN_PASSWORD=${GEOSERVER_ADMIN_PASSWORD}
      - GEOSERVER_ADMIN_USER=${GEOSERVER_ADMIN_USER}

      - TOMCAT_USER=${TOMCAT_USER}
      - TOMCAT_PASS=${TOMCAT_PASS}
      - STABLE_EXTENSIONS=importer-plugin


  emqx:
    image: emqx/emqx:latest
    container_name: emqx
    ports:
      - "1883:1883"
      - "8081:8081"
      - "18083:18083"
    # volumes:
    #   - emqx:/opt/emqx/etc
    #   - emqx:/opt/emqx/data
    #   - emqx:/opt/emqx/log
    environment:
      - EMQX_NAME=emqx
      - EMQX_HOST=${EMQX_HOST}
      - EMQX_NODE__DIST_LISTEN_MAX=6379
    restart: always
    networks:
      - wgo-network


volumes:
  geoserver-data:
  db:
    driver: local
  data:
    driver: local
  prometheus_data:

