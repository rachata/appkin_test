require('dotenv').config()
require("rootpath")();

const express = require('express');
const app = express();
const morgan = require("morgan");
const winston = require("winston");
const { combine, timestamp, printf } = winston.format;
const cors = require("cors");
const bodyParser = require("body-parser");
const upload = require("express-fileupload");
const errorHandler = require('./src/_helpers/error-handler');



const port = Number(process.env.REST_API_PORT);


const logFormat = printf(({ timestamp, level, message }) => {
  return `${timestamp} [${level.toUpperCase()}]: ${message}`;
});


let logger = winston.createLogger({
  level: "info", // Log level (e.g., 'info', 'error', 'warn')
  format: combine(timestamp(), logFormat), // Include timestamps in log entries
  transports: [
    new winston.transports.Console(), // Output logs to the console
    new winston.transports.File({ filename: "error.log", level: "error" }), // Log errors to a file
    new winston.transports.File({ filename: "combined.log" }), // Log all messages to another file
  ],
});

// Create a writable stream for Morgan
const stream = {
  write: (message) => {
    logger.info(message.trim()); // Trim and log the message
  },
};


// Middleware for request logging using Morgan
app.use(morgan("combined", { stream: stream }));



app.use(upload());

app.use(express.static('public'));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());




app.use("/index", require("./src/controller/index.controller.js"));
app.use("/datasource", require("./src/controller/datasource.controller.js"));

app.use(errorHandler);

app.listen(port, () => {
  logger.info(`Server listening on port ${port}`);
});

