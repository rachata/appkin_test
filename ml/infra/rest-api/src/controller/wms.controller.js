require("dotenv").config();

const express = require("express");
const axios = require("axios");
const { renameKey } = require("../_helpers/utility");

const router = express.Router();

router.get("/geocode", getAllGeocode);

router.get("/geocode/:geocode", getPropertiesANDGeometryByGeocode);

router.get("/geocode/:geocode/properties", getPropertiesByGeocode);
router.get("/geocode/:geocode/geometry", getGeometryByGeocode);




module.exports = router;






async function getPropertiesANDGeometryByGeocode(req, res, next) {
  try {
    const geocode = req.params.geocode;

    const url = process.env.WMS;
    const response = await axios.get(url);

    const jsonData = response.data;

    
    let properties = {};
    let geometry = {};
    for (let i = 0; i < jsonData["features"].length; i++) {
      if (jsonData["features"][i]["properties"]["GEOCODE"] == geocode) {
        properties = jsonData["features"][i]["properties"];
        geometry = jsonData["features"][i]["geometry"];
      }
    }

    if (properties != null) {
      renameKey(properties, "DistrictNE", "District");
      renameKey(properties, "PROV_E", "PROV");
      renameKey(properties, "TAMBON_E", "TAMBON");

      delete properties["DistrictNT"];
      delete properties["District_T"];
      delete properties["PROV_T"];
      delete properties["TAMBON_T"];


    }

    res.json({ properties: properties  , geometry :  geometry});
  } catch (error) {
    next(error);
  }
}


async function getGeometryByGeocode(req, res, next) {
  try {
    const geocode = req.params.geocode;

    const url = process.env.WMS;
      const response = await axios.get(url);


    const jsonData = response.data;

    let geometry = {};
    

    for (let i = 0; i < jsonData["features"].length; i++) {
      if (jsonData["features"][i]["properties"]["GEOCODE"] == geocode) {
        geometry = jsonData["features"][i]["geometry"];
      }
    }

  
    res.json({ geometry: geometry });
  } catch (error) {
    next(error);
  }
}


async function getPropertiesByGeocode(req, res, next) {
  try {
    const geocode = req.params.geocode;

    const url = process.env.WMS;
    
      const response = await axios.get(url);

    const jsonData = response.data;

    let properties = {};

    for (let i = 0; i < jsonData["features"].length; i++) {
      if (jsonData["features"][i]["properties"]["GEOCODE"] == geocode) {
        properties = jsonData["features"][i]["properties"];
      }
    }

    if (properties != null) {
      renameKey(properties, "DistrictNE", "District");
      renameKey(properties, "PROV_E", "PROV");
      renameKey(properties, "TAMBON_E", "TAMBON");

      delete properties["DistrictNT"];
      delete properties["District_T"];
      delete properties["PROV_T"];
      delete properties["TAMBON_T"];


    }

    res.json({ properties: properties });
  } catch (error) {
    next(error);
  }
}

async function getAllGeocode(req, res, next) {
  try {
    const url = process.env.WMS;
      const response = await axios.get(url);

    const jsonData = response.data;

    let features = [];

    for (let i = 0; i < jsonData["features"].length; i++) {
      features.push({
        GEOCODE: jsonData["features"][i]["properties"]["GEOCODE"],
        NAME: jsonData["features"][i]["properties"]["TAMBON_E"],
      });
    }

    features.sort((a, b) => a.GEOCODE - b.GEOCODE);

    res.json({ count: features.length, features: features });
  } catch (error) {
    next(error);
  }
}
