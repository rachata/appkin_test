require("dotenv").config();

const { sendToQueue } = require("../_helpers/amqp.js")


const express = require("express");
const { validateJSONKeysAndTypes } = require("../_helpers/utility");
const router = express.Router();
const { DateTime } = require('luxon');

router.post(
  "/scraping-salinity",
  validateJSONKeysAndTypes([
    { key: "stationID", type: "string" },
    { key: "temperature", type: "string" },
    { key: "salinity", type: "string" },
    { key: "waterLevel", type: "string" },
    { key: "rainfall", type: "string" },
    { key: "windDirection", type: "string" },
    { key: "windSpeed", type: "string" },
    { key: "tds", type: "string" },
    { key: "ec", type: "string" },
    { key: "dt", type: "string" },
  ]),

  datasourceSalinityScraping
);

router.post(
  "/salinity",
  validateJSONKeysAndTypes([
    { key: "stationID", type: "string" },
    { key: "temperature", type: "string" },
    { key: "salinity", type: "string" },
    { key: "waterLevel", type: "string" },
    { key: "rainfall", type: "string" },
    { key: "windDirection", type: "string" },
    { key: "windSpeed", type: "string" },
    { key: "tds", type: "string" },
    { key: "ec", type: "string" },
    { key: "dt", type: "string" },
  ]),

  datasourceSalinity
);

router.post(
  "/flow-rate",
  validateJSONKeysAndTypes([
    { key: "stationID", type: "string" },
    { key: "waterLevel", type: "string" },
    { key: "depth", type: "string" },
    { key: "waterFlowRate", type: "string" },
    { key: "dt", type: "string" },
  ]),

  datasourceFlowRate
);

router.post(
  "/scraping-flow-rate",
  validateJSONKeysAndTypes([
    { key: "stationID", type: "string" },
    { key: "waterLevel", type: "string" },
    { key: "depth", type: "string" },
    { key: "waterFlowRate", type: "string" },
    { key: "dt", type: "string" },
  ]),

  datasourceFlowRateScraping
);

module.exports = router;

async function datasourceFlowRateScraping(req, res, next) {

  const now = DateTime.now().setZone('Asia/Bangkok');
  const formattedDateTime = now.toFormat('yyyy-MM-dd HH:mm:ss');

  var raw = req.body;

  raw.dtServer = formattedDateTime

  // sendToQueue("scraping-flow-rate" , JSON.stringify(raw))

  res.json(raw);
}

async function datasourceFlowRate(req, res, next) {

  const now = DateTime.now().setZone('Asia/Bangkok');
  const formattedDateTime = now.toFormat('yyyy-MM-dd HH:mm:ss');

  var raw = req.body;

  raw.dtServer = formattedDateTime

  // sendToQueue("flow-rate" , JSON.stringify(raw))

  res.json(raw);
}

async function datasourceSalinityScraping(req, res, next) {

  const now = DateTime.now().setZone('Asia/Bangkok');
  const formattedDateTime = now.toFormat('yyyy-MM-dd HH:mm:ss');

  var raw = req.body;

  raw.dtServer = formattedDateTime
  
  
  
  // sendToQueue("scraping-salinity" , JSON.stringify(raw))

  res.json(raw);
}


async function datasourceSalinity(req, res, next) {

  const now = DateTime.now().setZone('Asia/Bangkok');
  const formattedDateTime = now.toFormat('yyyy-MM-dd HH:mm:ss');

  var raw = req.body;

  raw.dtServer = formattedDateTime
  
  
  
  // sendToQueue("salinity" , JSON.stringify(raw))

  res.json(raw);
}
