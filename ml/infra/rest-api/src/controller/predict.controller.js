require("dotenv").config();
const { DateTime } = require("luxon");
const express = require("express");
const { getRandomInt } = require("../_helpers/utility");
const axios = require("axios");

const router = express.Router();

router.get("/water-balance/daily/province/:geocode", getPredictWaterBalanceProvince);
router.get("/water-balance/daily/amphoe/:geocode", getPredictWaterBalanceAmphoe);
router.get("/water-balance/daily/tambol/:geocode", getPredictWaterBalanceTambol);
router.get("/water-balance/daily/plot/:geocode", getPredictWaterBalancePlot);

router.get("/water-balance/daily/plot",getPredictWaterBalanceAllPlot);


router.get("/water-balance/daily/province/",getPredictWaterBalanceAllProvince);
router.get("/water-balance/daily/amphoe/province/:geocode",getPredictWaterBalanceAllAmphoeProvince);
router.get("/water-balance/daily/tambol/province/:geocode",getPredictWaterBalanceAllTambolProvince);


router.get("/water-balance/monthly/province/:geocode", getMonthlyPredictWaterBalanceProvince);
router.get("/water-balance/monthly/amphoe/:geocode", getMonthlyPredictWaterBalanceAmphoe);
router.get("/water-balance/monthly/tambol/:geocode", getMonthlyPredictWaterBalanceTambol);

router.get("/water-balance/monthly/plot/:geocode", getMonthlyPredictWaterBalancePlot);

router.get("/water-balance/monthly/plot",getMonthlyPredictWaterBalanceAllPlot);


router.get("/water-balance/monthly/amphoe/province/:geocode",getMonthlyPredictWaterBalanceAllAmphoeProvince);
router.get("/water-balance/monthly/tambol/province/:geocode",getMonthlyPredictWaterBalanceAllTambolProvince);
router.get("/water-balance/monthly/province/",getMonthlyPredictWaterBalanceAllProvince);

router.get("/salinity/:geocode", getPredictSalinity);
router.get("/salinity", getAllPredictSalinity);

module.exports = router;

async function getMonthlyPredictWaterBalanceAllPlot(req, res, next) {
  try {


    const tambol = ["wgo:wt-plot-phatthalung"];

    let predicts = [];
    for (let i = 0; i < tambol.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${tambol[i]}&maxFeatures=500&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {
        

        let raw = {};


        raw['gecode'] = {
          geoCode: `${jsonData["features"][j]["properties"]["GEOCODE"]}`,
          PlotId : `${jsonData["features"][j]["properties"]["PlotId"]}`,
          name: jsonData["features"][j]["properties"]["Plotname"]

        }

        let predict = getMonthPredict();
        raw['predict'] = predict;
   


        predicts.push(raw)


      }
    }



    res.json({ total : predicts.length ,  predicts : predicts });
  } catch (error) {
    next(error);
  }
}


async function getMonthlyPredictWaterBalancePlot(req, res, next) {
  try {
    const geoCode = req.params.geocode;

    const tambol = ["wgo:wt-plot-phatthalung"];

    let geocode = [];
    for (let i = 0; i < tambol.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${tambol[i]}&maxFeatures=500&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {
        if (
          `${geoCode}` == `${jsonData["features"][j]["properties"]["PlotId"]}`
        ) {
          geocode = {
            geoCode: `${jsonData["features"][j]["properties"]["GEOCODE"]}`,
            PlotId : `${jsonData["features"][j]["properties"]["PlotId"]}`,
            name: jsonData["features"][j]["properties"]["Plotname"]

          };

          break;
        }
      }
    }

    let predict = getMonthPredict();

    res.json({ geocode: geocode, predict: predict });
  } catch (error) {
    next(error);
  }
}



async function getPredictWaterBalanceAllPlot(req, res, next) {
  try {


    const tambol = ["wgo:wt-plot-phatthalung"];

    let predicts = [];
    for (let i = 0; i < tambol.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${tambol[i]}&maxFeatures=500&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {
        

        let raw = {};


        raw['gecode'] = {
          geoCode: `${jsonData["features"][j]["properties"]["GEOCODE"]}`,
          PlotId : `${jsonData["features"][j]["properties"]["PlotId"]}`,
          name: jsonData["features"][j]["properties"]["Plotname"]

        }

        let predict = getPredict();
        raw['predict'] = predict;
   


        predicts.push(raw)


      }
    }



    res.json({ total : predicts.length ,  predicts : predicts });
  } catch (error) {
    next(error);
  }
}


async function getPredictWaterBalancePlot(req, res, next) {
  try {
    const geoCode = req.params.geocode;

    const tambol = ["wgo:wt-plot-phatthalung"];

    let geocode = [];
    for (let i = 0; i < tambol.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${tambol[i]}&maxFeatures=500&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {
        if (
          `${geoCode}` == `${jsonData["features"][j]["properties"]["PlotId"]}`
        ) {
          geocode = {
            geoCode: `${jsonData["features"][j]["properties"]["GEOCODE"]}`,
            PlotId : `${jsonData["features"][j]["properties"]["PlotId"]}`,
            name: jsonData["features"][j]["properties"]["Plotname"]

          };

          break;
        }
      }
    }

    let predict = getPredict();

    res.json({ geocode: geocode, predict: predict });
  } catch (error) {
    next(error);
  }
}



async function getAllPredictSalinity(req, res, next) {
  try {
    let yesterday = getRandomDoubleWithDecimalPlaces(0, 15.3, 2);
    let descriptionYesterday = checkSalinityDescription(yesterday);
    let colorYesterday = checkSalinityColor(yesterday);

    let daily = getRandomDoubleWithDecimalPlaces(0, 15.3, 2);
    let descriptionDaily = checkSalinityDescription(daily);
    let colorDaily = checkSalinityColor(daily);

    let today = getRandomDoubleWithDecimalPlaces(0, 15.3, 2);
    let tomorrow = getRandomDoubleWithDecimalPlaces(0, 15.3, 2);
    let afterTomorrow = getRandomDoubleWithDecimalPlaces(0, 15.3, 2);

    let rateDailyChange = ((daily - yesterday) / yesterday) * 100;

    let rateTodayChange = ((today - daily) / daily) * 100;

    let rateTomorrowChange = ((tomorrow - today) / today) * 100;

    let rateAfterTomorrowChange = ((afterTomorrow - tomorrow) / tomorrow) * 100;

    const geocode = req.params.geocode;

    const tambol = ["wgo:wt-tambol-phatthalung", "wgo:wt-tambol_ratchaburi"];


    let predicts = [];
    for (let i = 0; i < tambol.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${tambol[i]}&maxFeatures=200&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {

        let features = {};

          features = {
            geoCode: `${jsonData["features"][j]["properties"]["GEOCODE"]}`,
            name: jsonData["features"][j]["properties"]["TAMBON_E"],
          };

          let raw = {
            geocode: {
              geoCode: features["geoCode"],
              name: features["name"],
            },
      
            yesterday : {
              date : getDatePredict(-1),
              salinityYesterday: yesterday,
              descriptionYesterday: descriptionYesterday,
              colorYesterday: colorYesterday,
            },
           
      
            daily : {
              date : getDatePredict(0),
              salinityDaily: daily,
            descriptionDaily: descriptionDaily,
            colorDaily: colorDaily,
            rateDailyChange: parseFloat(rateDailyChange.toFixed(2)),
            },
      
            
      
            forecastToday : {
              date : getDatePredict(1),
              salinityForecastToday: today,
              rateTodayChange: parseFloat(rateTodayChange.toFixed(2)),
              descriptionToday: checkSalinityDescription(today),
              colorToday: checkSalinityColor(today),
            },
      
            forecastTomorrow : {
              date : getDatePredict(2),
              salinityForecastTomorrow: tomorrow,
              rateTomorrowChange: parseFloat(rateTomorrowChange.toFixed(2)),
              descriptionTomorrow: checkSalinityDescription(tomorrow),
              colorTomorrow: checkSalinityColor(tomorrow),
      
      
            },
      
           
            forecastDayAfterTomorrow : {
              date : getDatePredict(3),
              salinityForecastDayAfterTomorrow: afterTomorrow,
              rateAfterTomorrowChange: parseFloat(rateAfterTomorrowChange.toFixed(2)),
              descriptionDayAfterTomorrow: checkSalinityDescription(afterTomorrow),
              colorDayAfterTomorrow: checkSalinityColor(afterTomorrow),
            }
       
      
           
          };


          features['predict'] = raw;

          predicts.push(features);
  
        
      }
    }

   

    res.json({total : predicts.length, predicts : predicts});
  } catch (e) {
    next(e);
  }
}



async function getMonthlyPredictWaterBalanceAllProvince(req, res, next) {
  try {
 

    const province = [
      "wgo:wt-province-phatthalung",
      "wgo:wt-province_ratchaburi",
    ];

    let geocode = [];
    for (let i = 0; i < province.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${province[i]}&maxFeatures=20&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;


      let predict = getMonthPredict();

      let raw = {};

      raw["gecode"] = {
        geoCode: jsonData["features"][0]["properties"]["PROV_CODE"],
        nameThai: jsonData["features"][0]["properties"]["PROV_NAM_T"],
        nameEng: jsonData["features"][0]["properties"]["PROV_NAM_E"],
      };
      raw["predict"] = predict;

      geocode.push(raw);



    
    }



    res.json({ total : geocode.length ,  predicts: geocode });
  } catch (error) {
    next(error);
  }
}



async function getMonthlyPredictWaterBalanceAllTambolProvince(req, res, next) {
  try {
    const geoCode = req.params.geocode;

    const tambol = ["wgo:wt-tambol-phatthalung", "wgo:wt-tambol_ratchaburi"];

    let geocode = [];
    for (let i = 0; i < tambol.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${tambol[i]}&maxFeatures=500&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {
        
        let predict = getMonthPredict();



        
        if (
          `${geoCode}` ==
          `${jsonData["features"][j]["properties"]["PROV_CODE"]}`
        ) {

     
          let raw = {};

          raw["gecode"] = {
             geoCode: `${jsonData["features"][j]["properties"]["GEOCODE"]}`,
            nameThai: jsonData["features"][j]["properties"]["TAMBON_T"],
            nameEng: jsonData["features"][j]["properties"]["TAMBON_E"],
          };
          raw["predict"] = predict;

          geocode.push(raw);

 
        }

      }
    }


    res.json({ total : geocode.length ,  predicts: geocode});
  } catch (error) {
    next(error);
  }
}


async function getMonthlyPredictWaterBalanceAllAmphoeProvince(req, res, next) {
  try {
    const geoCode = req.params.geocode;

    const amphoe = ["wgo:wt-amphoe-phatthalung", "wgo:wt-amphoe_ratchaburi"];

    let geocode = [];
    for (let i = 0; i < amphoe.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${amphoe[i]}&maxFeatures=200&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {
        let predict = getMonthPredict();

        if (
          `${geoCode}` ==
          `${jsonData["features"][j]["properties"]["PROVINCE_C"]}`
        ) {
          let raw = {};

          raw["gecode"] = {
            geoCode: jsonData["features"][j]["properties"]["AMP_CODE"],
            nameThai: jsonData["features"][j]["properties"]["DistrictNT"],
            nameEng: jsonData["features"][j]["properties"]["DistrictNE"],
          };
          raw["predict"] = predict;

          geocode.push(raw);

 
        }
      }
      // (millimeter)  (cubic meter)
    }

    res.json({ total : geocode.length ,  predicts: geocode});
  } catch (error) {
    next(error);
  }
}


async function getMonthlyPredictWaterBalanceTambol(req, res, next) {
  try {
    const geoCode = req.params.geocode;

    const tambol = ["wgo:wt-tambol-phatthalung", "wgo:wt-tambol_ratchaburi"];

    let geocode = [];
    for (let i = 0; i < tambol.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${tambol[i]}&maxFeatures=500&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {
        if (
          `${geoCode}` == `${jsonData["features"][j]["properties"]["GEOCODE"]}`
        ) {
          geocode = {
            geoCode: `${jsonData["features"][j]["properties"]["GEOCODE"]}`,
            nameThai: jsonData["features"][j]["properties"]["TAMBON_T"],
            nameEng: jsonData["features"][j]["properties"]["TAMBON_E"],
          };

          break;
        }
      }
    }

    let predict = getMonthPredict();

    res.json({ geocode: geocode, predict: predict });
  } catch (error) {
    next(error);
  }
}




async function getMonthlyPredictWaterBalanceAmphoe(req, res, next) {
  try {
    const geoCode = req.params.geocode;

    const amphoe = ["wgo:wt-amphoe-phatthalung", "wgo:wt-amphoe_ratchaburi"];

    let geocode = [];
    for (let i = 0; i < amphoe.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${amphoe[i]}&maxFeatures=200&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {
        if (`${geoCode}` == jsonData["features"][j]["properties"]["AMP_CODE"]) {
          geocode = {
            geoCode: jsonData["features"][j]["properties"]["AMP_CODE"],
            nameThai: jsonData["features"][j]["properties"]["DistrictNT"],
            nameEng: jsonData["features"][j]["properties"]["DistrictNE"],
          };

          break;
        }
      }
    }

    let predict = getMonthPredict();

    res.json({ geocode: geocode, predict: predict });
  } catch (error) {
    next(error);
  }
}

async function getMonthlyPredictWaterBalanceProvince(req, res, next) {
  try {
    const geoCode = req.params.geocode;

    const province = [
      "wgo:wt-province-phatthalung",
      "wgo:wt-province_ratchaburi",
    ];

    let geocode = {};
    for (let i = 0; i < province.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${province[i]}&maxFeatures=20&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      if (`${geoCode}` == jsonData["features"][0]["properties"]["PROV_CODE"]) {
        geocode = {
          geoCode: jsonData["features"][0]["properties"]["PROV_CODE"],
          nameThai: jsonData["features"][0]["properties"]["PROV_NAM_T"],
          nameEng: jsonData["features"][0]["properties"]["PROV_NAM_E"],
        };
      }
    }

    let predict = getMonthPredict();

    res.json({ geocode: geocode, predict: predict });
  } catch (error) {
    next(error);
  }
}




async function getPredictWaterBalanceAllProvince(req, res, next) {
  try {
 

    const province = [
      "wgo:wt-province-phatthalung",
      "wgo:wt-province_ratchaburi",
    ];

    let geocode = [];
    for (let i = 0; i < province.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${province[i]}&maxFeatures=20&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;


      let predict = getPredict();

      let raw = {};

      raw["gecode"] = {
        geoCode: jsonData["features"][0]["properties"]["PROV_CODE"],
        nameThai: jsonData["features"][0]["properties"]["PROV_NAM_T"],
        nameEng: jsonData["features"][0]["properties"]["PROV_NAM_E"],
      };
      raw["predict"] = predict;

      geocode.push(raw);



    
    }



    res.json({ total : geocode.length ,  predicts: geocode });
  } catch (error) {
    next(error);
  }
}



async function getPredictWaterBalanceAllTambolProvince(req, res, next) {
  try {
    const geoCode = req.params.geocode;

    const tambol = ["wgo:wt-tambol-phatthalung", "wgo:wt-tambol_ratchaburi"];

    let geocode = [];
    for (let i = 0; i < tambol.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${tambol[i]}&maxFeatures=500&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {
        
        let predict = getPredict();



        
        if (
          `${geoCode}` ==
          `${jsonData["features"][j]["properties"]["PROV_CODE"]}`
        ) {

     
          let raw = {};

          raw["gecode"] = {
             geoCode: `${jsonData["features"][j]["properties"]["GEOCODE"]}`,
            nameThai: jsonData["features"][j]["properties"]["TAMBON_T"],
            nameEng: jsonData["features"][j]["properties"]["TAMBON_E"],
          };
          raw["predict"] = predict;

          geocode.push(raw);

 
        }

      }
    }


    res.json({ total : geocode.length ,  predicts: geocode});
  } catch (error) {
    next(error);
  }
}


async function getPredictWaterBalanceAllAmphoeProvince(req, res, next) {
  try {
    const geoCode = req.params.geocode;

    const amphoe = ["wgo:wt-amphoe-phatthalung", "wgo:wt-amphoe_ratchaburi"];

    let geocode = [];
    for (let i = 0; i < amphoe.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${amphoe[i]}&maxFeatures=200&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {
        let predict = getPredict();

        if (
          `${geoCode}` ==
          `${jsonData["features"][j]["properties"]["PROVINCE_C"]}`
        ) {
          let raw = {};

          raw["gecode"] = {
            geoCode: jsonData["features"][j]["properties"]["AMP_CODE"],
            nameThai: jsonData["features"][j]["properties"]["DistrictNT"],
            nameEng: jsonData["features"][j]["properties"]["DistrictNE"],
          };
          raw["predict"] = predict;

          geocode.push(raw);

 
        }
      }
      // (millimeter)  (cubic meter)
    }

    res.json({ total : geocode.length ,  predicts: geocode});
  } catch (error) {
    next(error);
  }
}

async function getPredictSalinity(req, res, next) {
  try {
    let yesterday = getRandomDoubleWithDecimalPlaces(0, 15.3, 2);
    let descriptionYesterday = checkSalinityDescription(yesterday);
    let colorYesterday = checkSalinityColor(yesterday);

    let daily = getRandomDoubleWithDecimalPlaces(0, 15.3, 2);
    let descriptionDaily = checkSalinityDescription(daily);
    let colorDaily = checkSalinityColor(daily);

    let today = getRandomDoubleWithDecimalPlaces(0, 15.3, 2);
    let tomorrow = getRandomDoubleWithDecimalPlaces(0, 15.3, 2);
    let afterTomorrow = getRandomDoubleWithDecimalPlaces(0, 15.3, 2);

    let rateDailyChange = ((daily - yesterday) / yesterday) * 100;

    let rateTodayChange = ((today - daily) / daily) * 100;

    let rateTomorrowChange = ((tomorrow - today) / today) * 100;

    let rateAfterTomorrowChange = ((afterTomorrow - tomorrow) / tomorrow) * 100;

    const geocode = req.params.geocode;

    const tambol = ["wgo:wt-tambol-phatthalung", "wgo:wt-tambol_ratchaburi"];

    let features = [];
    for (let i = 0; i < tambol.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${tambol[i]}&maxFeatures=200&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {
        if (`${geocode}` == jsonData["features"][j]["properties"]["GEOCODE"]) {
          features = {
            geoCode: `${jsonData["features"][j]["properties"]["GEOCODE"]}`,
            name: jsonData["features"][j]["properties"]["TAMBON_E"],
          };

          break;
        }
      }
    }

    let raw = {
      geocode: {
        geoCode: features["geoCode"],
        name: features["name"],
      },

      yesterday : {
        date : getDatePredict(-1),
        salinityYesterday: yesterday,
        descriptionYesterday: descriptionYesterday,
        colorYesterday: colorYesterday,
      },
     

      daily : {
        date : getDatePredict(0),
        salinityDaily: daily,
      descriptionDaily: descriptionDaily,
      colorDaily: colorDaily,
      rateDailyChange: parseFloat(rateDailyChange.toFixed(2)),
      },

      

      forecastToday : {
        date : getDatePredict(1),
        salinityForecastToday: today,
        rateTodayChange: parseFloat(rateTodayChange.toFixed(2)),
        descriptionToday: checkSalinityDescription(today),
        colorToday: checkSalinityColor(today),
      },

      forecastTomorrow : {
        date : getDatePredict(2),
        salinityForecastTomorrow: tomorrow,
        rateTomorrowChange: parseFloat(rateTomorrowChange.toFixed(2)),
        descriptionTomorrow: checkSalinityDescription(tomorrow),
        colorTomorrow: checkSalinityColor(tomorrow),


      },

     
      forecastDayAfterTomorrow : {
        date : getDatePredict(3),
        salinityForecastDayAfterTomorrow: afterTomorrow,
        rateAfterTomorrowChange: parseFloat(rateAfterTomorrowChange.toFixed(2)),
        descriptionDayAfterTomorrow: checkSalinityDescription(afterTomorrow),
        colorDayAfterTomorrow: checkSalinityColor(afterTomorrow),
      }
 

     
    };

    res.json(raw);
  } catch (e) {
    next(e);
  }
}

async function getPredictWaterBalanceTambol(req, res, next) {
  try {
    const geoCode = req.params.geocode;

    const tambol = ["wgo:wt-tambol-phatthalung", "wgo:wt-tambol_ratchaburi"];

    let geocode = [];
    for (let i = 0; i < tambol.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${tambol[i]}&maxFeatures=500&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {
        if (
          `${geoCode}` == `${jsonData["features"][j]["properties"]["GEOCODE"]}`
        ) {
          geocode = {
            geoCode: `${jsonData["features"][j]["properties"]["GEOCODE"]}`,
            nameThai: jsonData["features"][j]["properties"]["TAMBON_T"],
            nameEng: jsonData["features"][j]["properties"]["TAMBON_E"],
          };

          break;
        }
      }
    }

    let predict = getPredict();

    res.json({ geocode: geocode, predict: predict });
  } catch (error) {
    next(error);
  }
}

async function getPredictWaterBalanceAmphoe(req, res, next) {
  try {
    const geoCode = req.params.geocode;

    const amphoe = ["wgo:wt-amphoe-phatthalung", "wgo:wt-amphoe_ratchaburi"];

    let geocode = [];
    for (let i = 0; i < amphoe.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${amphoe[i]}&maxFeatures=200&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {
        if (`${geoCode}` == jsonData["features"][j]["properties"]["AMP_CODE"]) {
          geocode = {
            geoCode: jsonData["features"][j]["properties"]["AMP_CODE"],
            nameThai: jsonData["features"][j]["properties"]["DistrictNT"],
            nameEng: jsonData["features"][j]["properties"]["DistrictNE"],
          };

          break;
        }
      }
    }

    let predict = getPredict();

    res.json({ geocode: geocode, predict: predict });
  } catch (error) {
    next(error);
  }
}

async function getPredictWaterBalanceProvince(req, res, next) {
  try {
    const geoCode = req.params.geocode;

    const province = [
      "wgo:wt-province-phatthalung",
      "wgo:wt-province_ratchaburi",
    ];

    let geocode = {};
    for (let i = 0; i < province.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${province[i]}&maxFeatures=20&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      if (`${geoCode}` == jsonData["features"][0]["properties"]["PROV_CODE"]) {
        geocode = {
          geoCode: jsonData["features"][0]["properties"]["PROV_CODE"],
          nameThai: jsonData["features"][0]["properties"]["PROV_NAM_T"],
          nameEng: jsonData["features"][0]["properties"]["PROV_NAM_E"],
        };
      }
    }

    let predict = getPredict();

    res.json({ geocode: geocode, predict: predict });
  } catch (error) {
    next(error);
  }
}


function getMonthPredict() {
  var colorDrought = ["#FFFFFF", "#F4ED0B", "#F16910", "#EF1212"];

  var textDrought = ["ไม่เสี่ยง", "เสี่ยงน้อย", "เสี่ยงปานกลาง", "เสี่ยงมาก"];

  var colorFlood = ["#FFFFFF", "#7ADEFC", "#2A81FE", "#0048AF"];

  var textFlood = ["ไม่เสี่ยง", "เสี่ยงน้อย", "เสี่ยงปานกลาง", "เสี่ยงมาก"];

  let forecastMonth01 = {
    date: getDateMonthPredict(0),
    dailyRainfall: getRandomDoubleWithDecimalPlaces(0, 100, 2),
    evapotranspiration: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    waterShortage: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    drought: getRandomInt(0, 3),
    flood: getRandomInt(0, 3),
  };

  forecastMonth01["colorDrought"] = colorDrought[forecastMonth01["drought"]];
  forecastMonth01["descriptionDrought"] = textDrought[forecastMonth01["drought"]];

  forecastMonth01["colorFlood"] = colorFlood[forecastMonth01["flood"]];
  forecastMonth01["descriptionFlood"] = textFlood[forecastMonth01["flood"]];

  forecastMonth01["colorRainfall"] = checkRainColor(
    forecastMonth01["dailyRainfall"]
  );
  forecastMonth01["descriptionRainfall"] = checkRainDescription(
    forecastMonth01["dailyRainfall"]
  );

  let forecastMonth02 = {
    date: getDateMonthPredict(1),
    dailyRainfall: getRandomDoubleWithDecimalPlaces(0, 100, 2),
    evapotranspiration: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    waterShortage: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    drought: getRandomInt(0, 3),
    flood: getRandomInt(0, 3),
  };

  forecastMonth02["colorDrought"] = colorDrought[forecastMonth02["drought"]];
  forecastMonth02["descriptionDrought"] = textDrought[forecastMonth02["drought"]];

  forecastMonth02["colorFlood"] = colorFlood[forecastMonth02["flood"]];
  forecastMonth02["descriptionFlood"] = textFlood[forecastMonth02["flood"]];

  forecastMonth02["colorRainfall"] = checkRainColor(
    forecastMonth02["dailyRainfall"]
  );
  forecastMonth02["descriptionRainfall"] = checkRainDescription(
    forecastMonth02["dailyRainfall"]
  );

  let forecastMonth03 = {
    date: getDateMonthPredict(2),
    dailyRainfall: getRandomDoubleWithDecimalPlaces(0, 100, 2),
    evapotranspiration: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    waterShortage: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    drought: getRandomInt(0, 3),
    flood: getRandomInt(0, 3),
  };

  forecastMonth03["colorDrought"] = colorDrought[forecastMonth03["drought"]];
  forecastMonth03["descriptionDrought"] = textDrought[forecastMonth03["drought"]];

  forecastMonth03["colorFlood"] = colorFlood[forecastMonth03["flood"]];
  forecastMonth03["descriptionFlood"] = textFlood[forecastMonth03["flood"]];

  forecastMonth03["colorRainfall"] = checkRainColor(
    forecastMonth03["dailyRainfall"]
  );
  forecastMonth03["descriptionRainfall"] = checkRainDescription(
    forecastMonth03["dailyRainfall"]
  );

  let forecastMonth04 = {
    date: getDateMonthPredict(3),
    dailyRainfall: getRandomDoubleWithDecimalPlaces(0, 100, 2),
    evapotranspiration: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    waterShortage: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    drought: getRandomInt(0, 3),
    flood: getRandomInt(0, 3),
  };

  forecastMonth04["colorDrought"] = colorDrought[forecastMonth04["drought"]];
  forecastMonth04["descriptionDrought"] = textDrought[forecastMonth04["drought"]];

  forecastMonth04["colorFlood"] = colorFlood[forecastMonth04["flood"]];
  forecastMonth04["descriptionFlood"] = textFlood[forecastMonth04["flood"]];

  forecastMonth04["colorRainfall"] = checkRainColor(
    forecastMonth04["dailyRainfall"]
  );
  forecastMonth04["descriptionRainfall"] = checkRainDescription(
    forecastMonth04["dailyRainfall"]
  );

  let forecastMonth05 = {
    date: getDateMonthPredict(4),
    dailyRainfall: getRandomDoubleWithDecimalPlaces(0, 100, 2),
    evapotranspiration: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    waterShortage: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    drought: getRandomInt(0, 3),
    flood: getRandomInt(0, 3),
  };

  forecastMonth05["colorDrought"] = colorDrought[forecastMonth05["drought"]];
  forecastMonth05["descriptionDrought"] = textDrought[forecastMonth05["drought"]];

  forecastMonth05["colorFlood"] = colorFlood[forecastMonth05["flood"]];
  forecastMonth05["descriptionFlood"] = textFlood[forecastMonth05["flood"]];

  forecastMonth05["colorRainfall"] = checkRainColor(
    forecastMonth05["dailyRainfall"]
  );
  forecastMonth05["descriptionRainfall"] = checkRainDescription(
    forecastMonth05["dailyRainfall"]
  );

  let forecastMonth06 = {
    date: getDateMonthPredict(5),
    dailyRainfall: getRandomDoubleWithDecimalPlaces(0, 100, 2),
    evapotranspiration: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    waterShortage: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    drought: getRandomInt(0, 3),
    flood: getRandomInt(0, 3),
  };

  forecastMonth06["colorDrought"] = colorDrought[forecastMonth06["drought"]];
  forecastMonth06["descriptionDrought"] = textDrought[forecastMonth06["drought"]];

  forecastMonth06["colorFlood"] = colorFlood[forecastMonth06["flood"]];
  forecastMonth06["descriptionFlood"] = textFlood[forecastMonth06["flood"]];

  forecastMonth06["colorRainfall"] = checkRainColor(
    forecastMonth06["dailyRainfall"]
  );
  forecastMonth06["descriptionRainfall"] = checkRainDescription(
    forecastMonth06["dailyRainfall"]
  );



  let predict = {
    forecastMonth01: forecastMonth01,
    forecastMonth02: forecastMonth02,
    forecastMonth03: forecastMonth03,
    forecastMonth04: forecastMonth04,
    forecastMonth05: forecastMonth05,
    forecastMonth06: forecastMonth06
  };

  return predict;
}

function getPredict() {
  var colorDrought = ["#FFFFFF", "#F4ED0B", "#F16910", "#EF1212"];

  var textDrought = ["ไม่เสี่ยง", "เสี่ยงน้อย", "เสี่ยงปานกลาง", "เสี่ยงมาก"];

  var colorFlood = ["#FFFFFF", "#7ADEFC", "#2A81FE", "#0048AF"];

  var textFlood = ["ไม่เสี่ยง", "เสี่ยงน้อย", "เสี่ยงปานกลาง", "เสี่ยงมาก"];

  let forecastDay01 = {
    date: getDatePredict(0),
    dailyRainfall: getRandomDoubleWithDecimalPlaces(0, 100, 2),
    evapotranspiration: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    waterShortage: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    drought: getRandomInt(0, 3),
    flood: getRandomInt(0, 3),
  };

  forecastDay01["colorDrought"] = colorDrought[forecastDay01["drought"]];
  forecastDay01["descriptionDrought"] = textDrought[forecastDay01["drought"]];

  forecastDay01["colorFlood"] = colorFlood[forecastDay01["flood"]];
  forecastDay01["descriptionFlood"] = textFlood[forecastDay01["flood"]];

  forecastDay01["colorRainfall"] = checkRainColor(
    forecastDay01["dailyRainfall"]
  );
  forecastDay01["descriptionRainfall"] = checkRainDescription(
    forecastDay01["dailyRainfall"]
  );

  let forecastDay02 = {
    date: getDatePredict(1),
    dailyRainfall: getRandomDoubleWithDecimalPlaces(0, 100, 2),
    evapotranspiration: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    waterShortage: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    drought: getRandomInt(0, 3),
    flood: getRandomInt(0, 3),
  };

  forecastDay02["colorDrought"] = colorDrought[forecastDay02["drought"]];
  forecastDay02["descriptionDrought"] = textDrought[forecastDay02["drought"]];

  forecastDay02["colorFlood"] = colorFlood[forecastDay02["flood"]];
  forecastDay02["descriptionFlood"] = textFlood[forecastDay02["flood"]];

  forecastDay02["colorRainfall"] = checkRainColor(
    forecastDay02["dailyRainfall"]
  );
  forecastDay02["descriptionRainfall"] = checkRainDescription(
    forecastDay02["dailyRainfall"]
  );

  let forecastDay03 = {
    date: getDatePredict(2),
    dailyRainfall: getRandomDoubleWithDecimalPlaces(0, 100, 2),
    evapotranspiration: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    waterShortage: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    drought: getRandomInt(0, 3),
    flood: getRandomInt(0, 3),
  };

  forecastDay03["colorDrought"] = colorDrought[forecastDay03["drought"]];
  forecastDay03["descriptionDrought"] = textDrought[forecastDay03["drought"]];

  forecastDay03["colorFlood"] = colorFlood[forecastDay03["flood"]];
  forecastDay03["descriptionFlood"] = textFlood[forecastDay03["flood"]];

  forecastDay03["colorRainfall"] = checkRainColor(
    forecastDay03["dailyRainfall"]
  );
  forecastDay03["descriptionRainfall"] = checkRainDescription(
    forecastDay03["dailyRainfall"]
  );

  let forecastDay04 = {
    date: getDatePredict(3),
    dailyRainfall: getRandomDoubleWithDecimalPlaces(0, 100, 2),
    evapotranspiration: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    waterShortage: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    drought: getRandomInt(0, 3),
    flood: getRandomInt(0, 3),
  };

  forecastDay04["colorDrought"] = colorDrought[forecastDay04["drought"]];
  forecastDay04["descriptionDrought"] = textDrought[forecastDay04["drought"]];

  forecastDay04["colorFlood"] = colorFlood[forecastDay04["flood"]];
  forecastDay04["descriptionFlood"] = textFlood[forecastDay04["flood"]];

  forecastDay04["colorRainfall"] = checkRainColor(
    forecastDay04["dailyRainfall"]
  );
  forecastDay04["descriptionRainfall"] = checkRainDescription(
    forecastDay04["dailyRainfall"]
  );

  let forecastDay05 = {
    date: getDatePredict(4),
    dailyRainfall: getRandomDoubleWithDecimalPlaces(0, 100, 2),
    evapotranspiration: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    waterShortage: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    drought: getRandomInt(0, 3),
    flood: getRandomInt(0, 3),
  };

  forecastDay05["colorDrought"] = colorDrought[forecastDay05["drought"]];
  forecastDay05["descriptionDrought"] = textDrought[forecastDay05["drought"]];

  forecastDay05["colorFlood"] = colorFlood[forecastDay05["flood"]];
  forecastDay05["descriptionFlood"] = textFlood[forecastDay05["flood"]];

  forecastDay05["colorRainfall"] = checkRainColor(
    forecastDay05["dailyRainfall"]
  );
  forecastDay05["descriptionRainfall"] = checkRainDescription(
    forecastDay05["dailyRainfall"]
  );

  let forecastDay06 = {
    date: getDatePredict(5),
    dailyRainfall: getRandomDoubleWithDecimalPlaces(0, 100, 2),
    evapotranspiration: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    waterShortage: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    drought: getRandomInt(0, 3),
    flood: getRandomInt(0, 3),
  };

  forecastDay06["colorDrought"] = colorDrought[forecastDay06["drought"]];
  forecastDay06["descriptionDrought"] = textDrought[forecastDay06["drought"]];

  forecastDay06["colorFlood"] = colorFlood[forecastDay06["flood"]];
  forecastDay06["descriptionFlood"] = textFlood[forecastDay06["flood"]];

  forecastDay06["colorRainfall"] = checkRainColor(
    forecastDay06["dailyRainfall"]
  );
  forecastDay06["descriptionRainfall"] = checkRainDescription(
    forecastDay06["dailyRainfall"]
  );

  let forecastDay07 = {
    date: getDatePredict(6),
    dailyRainfall: getRandomDoubleWithDecimalPlaces(0, 100, 2),
    evapotranspiration: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    waterShortage: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    drought: getRandomInt(0, 3),
    flood: getRandomInt(0, 3),
  };

  forecastDay07["colorDrought"] = colorDrought[forecastDay07["drought"]];
  forecastDay07["descriptionDrought"] = textDrought[forecastDay07["drought"]];

  forecastDay07["colorFlood"] = colorFlood[forecastDay07["flood"]];
  forecastDay07["descriptionFlood"] = textFlood[forecastDay07["flood"]];

  forecastDay07["colorRainfall"] = checkRainColor(
    forecastDay07["dailyRainfall"]
  );
  forecastDay07["descriptionRainfall"] = checkRainDescription(
    forecastDay07["dailyRainfall"]
  );

  let predict = {
    forecastDay01: forecastDay01,
    forecastDay02: forecastDay02,
    forecastDay03: forecastDay03,
    forecastDay04: forecastDay04,
    forecastDay05: forecastDay05,
    forecastDay06: forecastDay06,
    forecastDay07: forecastDay07,
  };

  return predict;
}

function checkRainColor(rain) {
  if (rain >= 0 && rain <= 10.0) {
    return "#6DA9FF";
  } else if (rain >= 10.1 && rain <= 35.0) {
    return "#20A211";
  } else if (rain >= 35.1 && rain <= 90.1) {
    return "#FF4900";
  } else if (rain > 90.1) {
    return "#FF0000";
  }
}

function checkRainDescription(rain) {
  if (rain >= 0 && rain <= 10.0) {
    return "ฝนเล็กน้อย(Light Rain) ";
  } else if (rain >= 10.1 && rain <= 35.0) {
    return "ฝนเล็กน้อย(Light Rain) ";
  } else if (rain >= 35.1 && rain <= 90.1) {
    return "ฝนหนัก(Heavy Rain)";
  } else if (rain > 90.1) {
    return "ฝนหนักมาก(Very Heavy Rain)";
  }
}

function checkSalinityDescription(salinity) {
  if (salinity <= 0.2) {
    return "น้ำคุณภาพดี ";
  } else if (salinity >= 0.21 && salinity <= 0.5) {
    return "น้ำคุณภาพปานกลาง ";
  } else if (salinity >= 0.51 && salinity <= 1.5) {
    return "น้ำคุณภาพต่ำ มีเกลือมาก";
  } else if (salinity > 1.51) {
    return "น้ำคุณภาพต่ำมาก";
  }
}

function checkSalinityColor(salinity) {
  if (salinity <= 0.2) {
    return "#0B9BF3";
  } else if (salinity >= 0.21 && salinity <= 0.5) {
    return "#07CD55";
  } else if (salinity >= 0.51 && salinity <= 1.5) {
    return "#FFE804";
  } else if (salinity > 1.51) {
    return "#FF0404";
  }
}

function getDatePredict(next) {
  // Get the current date and time
  const now = DateTime.now();

  // Calculate the date that is 1 day from now
  const tomorrow = now.plus({ days: next });

  // Calculate the date that is 7 days from tomorrow
  // const sevenDaysFromTomorrow = tomorrow.plus({ days: 1 });

  const sevenDaysFromTomorrow = tomorrow;

  // Format the date as "yyyy-MM-dd"
  const formattedDate = sevenDaysFromTomorrow.toFormat("yyyy-MM-dd");

  return formattedDate;
}

function getDateMonthPredict(next) {
  // Get the current date and time
  const now = DateTime.now();

  // Calculate the date that is 'next' months from now
  const futureDate = now.plus({ months: next });

  // Format the date as "yyyy-MM-dd"
  const formattedDate = futureDate.toFormat("yyyy-MM-00");

  return formattedDate;
}

function getRandomDoubleWithDecimalPlaces(min, max, decimalPlaces) {
  const randomDouble = Math.random() * (max - min) + min;
  return parseFloat(randomDouble.toFixed(decimalPlaces));
}


function getSaPredict() {
  var colorDrought = ["#FFFFFF", "#F4ED0B", "#F16910", "#EF1212"];

  var textDrought = ["ไม่เสี่ยง", "เสี่ยงน้อย", "เสี่ยงปานกลาง", "เสี่ยงมาก"];

  var colorFlood = ["#FFFFFF", "#7ADEFC", "#2A81FE", "#0048AF"];

  var textFlood = ["ไม่เสี่ยง", "เสี่ยงน้อย", "เสี่ยงปานกลาง", "เสี่ยงมาก"];

  let forecastDay01 = {
    date: getDatePredict(0),
    dailyRainfall: getRandomDoubleWithDecimalPlaces(0, 100, 2),
    evapotranspiration: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    waterShortage: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    drought: getRandomInt(0, 3),
    flood: getRandomInt(0, 3),
  };

  forecastDay01["colorDrought"] = colorDrought[forecastDay01["drought"]];
  forecastDay01["descriptionDrought"] = textDrought[forecastDay01["drought"]];

  forecastDay01["colorFlood"] = colorFlood[forecastDay01["flood"]];
  forecastDay01["descriptionFlood"] = textFlood[forecastDay01["flood"]];

  forecastDay01["colorRainfall"] = checkRainColor(
    forecastDay01["dailyRainfall"]
  );
  forecastDay01["descriptionRainfall"] = checkRainDescription(
    forecastDay01["dailyRainfall"]
  );

  let forecastDay02 = {
    date: getDatePredict(1),
    dailyRainfall: getRandomDoubleWithDecimalPlaces(0, 100, 2),
    evapotranspiration: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    waterShortage: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    drought: getRandomInt(0, 3),
    flood: getRandomInt(0, 3),
  };

  forecastDay02["colorDrought"] = colorDrought[forecastDay02["drought"]];
  forecastDay02["descriptionDrought"] = textDrought[forecastDay02["drought"]];

  forecastDay02["colorFlood"] = colorFlood[forecastDay02["flood"]];
  forecastDay02["descriptionFlood"] = textFlood[forecastDay02["flood"]];

  forecastDay02["colorRainfall"] = checkRainColor(
    forecastDay02["dailyRainfall"]
  );
  forecastDay02["descriptionRainfall"] = checkRainDescription(
    forecastDay02["dailyRainfall"]
  );

  let forecastDay03 = {
    date: getDatePredict(2),
    dailyRainfall: getRandomDoubleWithDecimalPlaces(0, 100, 2),
    evapotranspiration: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    waterShortage: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    drought: getRandomInt(0, 3),
    flood: getRandomInt(0, 3),
  };

  forecastDay03["colorDrought"] = colorDrought[forecastDay03["drought"]];
  forecastDay03["descriptionDrought"] = textDrought[forecastDay03["drought"]];

  forecastDay03["colorFlood"] = colorFlood[forecastDay03["flood"]];
  forecastDay03["descriptionFlood"] = textFlood[forecastDay03["flood"]];

  forecastDay03["colorRainfall"] = checkRainColor(
    forecastDay03["dailyRainfall"]
  );
  forecastDay03["descriptionRainfall"] = checkRainDescription(
    forecastDay03["dailyRainfall"]
  );

  let forecastDay04 = {
    date: getDatePredict(3),
    dailyRainfall: getRandomDoubleWithDecimalPlaces(0, 100, 2),
    evapotranspiration: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    waterShortage: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    drought: getRandomInt(0, 3),
    flood: getRandomInt(0, 3),
  };

  forecastDay04["colorDrought"] = colorDrought[forecastDay04["drought"]];
  forecastDay04["descriptionDrought"] = textDrought[forecastDay04["drought"]];

  forecastDay04["colorFlood"] = colorFlood[forecastDay04["flood"]];
  forecastDay04["descriptionFlood"] = textFlood[forecastDay04["flood"]];

  forecastDay04["colorRainfall"] = checkRainColor(
    forecastDay04["dailyRainfall"]
  );
  forecastDay04["descriptionRainfall"] = checkRainDescription(
    forecastDay04["dailyRainfall"]
  );

  let forecastDay05 = {
    date: getDatePredict(4),
    dailyRainfall: getRandomDoubleWithDecimalPlaces(0, 100, 2),
    evapotranspiration: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    waterShortage: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    drought: getRandomInt(0, 3),
    flood: getRandomInt(0, 3),
  };

  forecastDay05["colorDrought"] = colorDrought[forecastDay05["drought"]];
  forecastDay05["descriptionDrought"] = textDrought[forecastDay05["drought"]];

  forecastDay05["colorFlood"] = colorFlood[forecastDay05["flood"]];
  forecastDay05["descriptionFlood"] = textFlood[forecastDay05["flood"]];

  forecastDay05["colorRainfall"] = checkRainColor(
    forecastDay05["dailyRainfall"]
  );
  forecastDay05["descriptionRainfall"] = checkRainDescription(
    forecastDay05["dailyRainfall"]
  );

  let forecastDay06 = {
    date: getDatePredict(5),
    dailyRainfall: getRandomDoubleWithDecimalPlaces(0, 100, 2),
    evapotranspiration: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    waterShortage: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    drought: getRandomInt(0, 3),
    flood: getRandomInt(0, 3),
  };

  forecastDay06["colorDrought"] = colorDrought[forecastDay06["drought"]];
  forecastDay06["descriptionDrought"] = textDrought[forecastDay06["drought"]];

  forecastDay06["colorFlood"] = colorFlood[forecastDay06["flood"]];
  forecastDay06["descriptionFlood"] = textFlood[forecastDay06["flood"]];

  forecastDay06["colorRainfall"] = checkRainColor(
    forecastDay06["dailyRainfall"]
  );
  forecastDay06["descriptionRainfall"] = checkRainDescription(
    forecastDay06["dailyRainfall"]
  );

  let forecastDay07 = {
    date: getDatePredict(6),
    dailyRainfall: getRandomDoubleWithDecimalPlaces(0, 100, 2),
    evapotranspiration: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    waterShortage: getRandomDoubleWithDecimalPlaces(0, 99999999, 2),
    drought: getRandomInt(0, 3),
    flood: getRandomInt(0, 3),
  };

  forecastDay07["colorDrought"] = colorDrought[forecastDay07["drought"]];
  forecastDay07["descriptionDrought"] = textDrought[forecastDay07["drought"]];

  forecastDay07["colorFlood"] = colorFlood[forecastDay07["flood"]];
  forecastDay07["descriptionFlood"] = textFlood[forecastDay07["flood"]];

  forecastDay07["colorRainfall"] = checkRainColor(
    forecastDay07["dailyRainfall"]
  );
  forecastDay07["descriptionRainfall"] = checkRainDescription(
    forecastDay07["dailyRainfall"]
  );

  let predict = {
    forecastDay01: forecastDay01,
    forecastDay02: forecastDay02,
    forecastDay03: forecastDay03,
    forecastDay04: forecastDay04,
    forecastDay05: forecastDay05,
    forecastDay06: forecastDay06,
    forecastDay07: forecastDay07,
  };

  return predict;
}