require("dotenv").config();

const express = require("express");
const axios = require("axios");
const {  sortByGeoCode } = require("../_helpers/utility");

const router = express.Router();

router.get("/water-balance/province/geocode",getAllGeocodeProvinceWaterBalance);
router.get("/water-balance/amphoe/geocode", getAllGeocodeAmphoeWaterBalance);
router.get("/water-balance/tambol/geocode", getAllGeocodeTambolWaterBalance);

router.get("/water-balance/plot/geocode", getAllGeocodePlotWaterBalance);


router.get("/water-balance/amphoe/geocode/:geocodeProvince", getAllGeocodeAmphoeWaterBalanceByGeocodeProvince);
router.get("/water-balance/tambol/geocode/:geocodeAmphoe", getAllGeocodeAmphoeWaterBalanceByGeocodeAmphoe);


router.get("/water-balance/province/features/:geocodeProvince",getAllFeaturesProvinceWaterBalance);
router.get("/water-balance/amphoe/features/:geocodeAmphoe",getAllFeaturesAmphoeWaterBalance);
router.get("/water-balance/tambol/features/:geocodeTambol",getAllFeaturesTambolWaterBalance);

router.get("/water-balance/plot/features/:plotID",getAllFeaturesPlotWaterBalance);


router.get("/salinity/geocode", getAllGeocodeTambolSalinity);
router.get("/salinity/features/:geocodeTambol",getAllFeaturesTambolSalinity);

module.exports = router;

async function getAllFeaturesPlotWaterBalance(req, res, next) {
  try {

    let plotID = req.params.plotID;

    const province = [
      "wgo:wt-plot-phatthalung"
    ];

    let features = {};
    for (let i = 0; i < province.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${province[i]}&maxFeatures=20&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;


      for(let j=0; j < jsonData["features"].length ; j++){

        console.log(`${plotID}` + " " + `${jsonData["features"][j]["properties"]["PlotId"]}`)

        if(`${plotID}` == `${jsonData["features"][j]["properties"]["PlotId"]}`){
  
          features = {
            "geometry" : jsonData["features"][j]['geometry'],
            "properties" : jsonData["features"][j]['properties']
          }
  
          break;
        }
    
        
      }
     
    }



    res.json({ features : features});
  } catch (error) {
    next(error);
  }
}



async function getAllGeocodePlotWaterBalance(req, res, next) {
  try {
    const tambol = ["wgo:wt-plot-phatthalung"];

    let geocode = [];
    for (let i = 0; i < tambol.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${tambol[i]}&maxFeatures=500&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {
        geocode.push({
          PlotId : `${jsonData["features"][j]["properties"]["PlotId"]}`,
          geoCode: `${jsonData["features"][j]["properties"]["GEOCODE"]}`,
          name: jsonData["features"][j]["properties"]["Plotname"]
        });
      }
    }

    geocode.sort(sortByGeoCode);

    res.json({ total : geocode.length , geocode: geocode });
  } catch (error) {
    next(error);
  }
}


async function getAllFeaturesTambolSalinity(req, res, next) {
  try {
    const tambol = [
      "wgo:salinity-district"
    ];


    let geocodeTambol = req.params.geocodeTambol;

    let features = {};
    for (let i = 0; i < tambol.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${tambol[i]}&maxFeatures=200&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {

       
        if(`${geocodeTambol}` == jsonData["features"][j]["properties"]["GEOCODE"]){


          features = {
            "geometry" : jsonData["features"][j]['geometry'],
            "properties" : jsonData["features"][j]['properties']
          }
  
          break;
        }

      
      }
     
    }


    delete features['properties']["DistrictNT"];
    delete features['properties']["District_T"];
    delete features['properties']["PROV_T"];
    delete features['properties']["TAMBON_T"];




    res.json({ features : features });
  } catch (error) {
    next(error);
  }
}

async function getAllGeocodeTambolSalinity(req, res, next) {
  try {
    const province = [
      "wgo:salinity-district"
    ];

    let geocode = [];
    for (let i = 0; i < province.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${province[i]}&maxFeatures=200&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for(let j = 0; j < jsonData["features"].length; j++){

        geocode.push({
          geoCode: `${jsonData["features"][j]["properties"]["GEOCODE"]}`,
          name: jsonData["features"][j]["properties"]["TAMBON_E"],
        });


      }
     
    }

    geocode.sort(sortByGeoCode);

    res.json({ total : geocode.length , geocode: geocode });
  } catch (error) {
    next(error);
  }
}



async function getAllFeaturesTambolWaterBalance(req, res, next) {
  try {

    const geocodeTambol = req.params.geocodeTambol;

    const tambol = ["wgo:wt-tambol-phatthalung", "wgo:wt-tambol_ratchaburi"];

    let features = [];
    for (let i = 0; i < tambol.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${tambol[i]}&maxFeatures=200&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {

       
        if(`${geocodeTambol}` == jsonData["features"][j]["properties"]["GEOCODE"]){

          features = {
            "geometry" : jsonData["features"][j]['geometry'],
            "properties" : jsonData["features"][j]['properties']
          }
  
          break;
        }

      
      }
    }



    res.json({ features : features });
  } catch (error) {
    next(error);
  }
}




async function getAllFeaturesAmphoeWaterBalance(req, res, next) {
  try {
    const amphoe = ["wgo:wt-amphoe-phatthalung", "wgo:wt-amphoe_ratchaburi"];

    let features = {}

    let geocodeAmphoe = req.params.geocodeAmphoe;

    for (let i = 0; i < amphoe.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${amphoe[i]}&maxFeatures=200&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {

        if(`${geocodeAmphoe}` == jsonData["features"][j]["properties"]["AMP_CODE"]){

          features = {
            "geometry" : jsonData["features"][j]['geometry'],
            "properties" : jsonData["features"][j]['properties']
          }
  
          break;
        }

      
      }
    }



    res.json({ features : features });
  } catch (error) {
    next(error);
  }
}



async function getAllFeaturesProvinceWaterBalance(req, res, next) {
  try {

    let geocodeProvince = req.params.geocodeProvince;

    const province = [
      "wgo:wt-province-phatthalung",
      "wgo:wt-province_ratchaburi",
    ];

    let features = {};
    for (let i = 0; i < province.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${province[i]}&maxFeatures=20&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;


      if(`${geocodeProvince}` == jsonData["features"][0]["properties"]["PROV_CODE"]){

        features = {
          "geometry" : jsonData["features"][0]['geometry'],
          "properties" : jsonData["features"][0]['properties']
        }

        break;
      }
    
    }



    delete features['properties']['P_CODE']
    delete features['properties']['REGION']
    delete features['properties']['R']

    res.json({ features : features});
  } catch (error) {
    next(error);
  }
}

async function getAllGeocodeAmphoeWaterBalanceByGeocodeAmphoe(req, res, next) {
  try {

    const geocodeAmphoe = req.params.geocodeAmphoe;

    const tambol = ["wgo:wt-tambol-phatthalung", "wgo:wt-tambol_ratchaburi"];

    let geocode = [];
    for (let i = 0; i < tambol.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${tambol[i]}&maxFeatures=200&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {

        if(geocodeAmphoe == `${jsonData["features"][j]["properties"]['AMPHOE_IDN']}`){
          geocode.push({
            geoCode: `${jsonData["features"][j]["properties"]["GEOCODE"]}`,
            nameThai: jsonData["features"][j]["properties"]["TAMBON_T"],
            nameEng: jsonData["features"][j]["properties"]["TAMBON_E"],
          });
        }
      
      }
    }

    geocode.sort(sortByGeoCode);

    res.json({ total : geocode.length ,  geocode: geocode });
  } catch (error) {
    next(error);
  }
}



async function getAllGeocodeAmphoeWaterBalanceByGeocodeProvince(req, res, next) {
  try {

    const geocodeProvince = req.params.geocodeProvince;

    const amphoe = ["wgo:wt-amphoe-phatthalung", "wgo:wt-amphoe_ratchaburi"];

    let geocode = [];
    for (let i = 0; i < amphoe.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${amphoe[i]}&maxFeatures=200&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {

        if(geocodeProvince == `${jsonData["features"][j]["properties"]['PROVINCE_C']}`){
          geocode.push({
            geoCode: jsonData["features"][j]["properties"]["AMP_CODE"],
            nameThai: jsonData["features"][j]["properties"]["DistrictNT"],
            nameEng: jsonData["features"][j]["properties"]["DistrictNE"],
          });
        }
      
      }
    }

    geocode.sort(sortByGeoCode);

    res.json({ total : geocode.length ,  geocode: geocode });
  } catch (error) {
    next(error);
  }
}



async function getAllGeocodeTambolWaterBalance(req, res, next) {
  try {
    const tambol = ["wgo:wt-tambol-phatthalung", "wgo:wt-tambol_ratchaburi"];

    let geocode = [];
    for (let i = 0; i < tambol.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${tambol[i]}&maxFeatures=500&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {
        geocode.push({
          geoCode: `${jsonData["features"][j]["properties"]["GEOCODE"]}`,
          nameThai: jsonData["features"][j]["properties"]["TAMBON_T"],
          nameEng: jsonData["features"][j]["properties"]["TAMBON_E"],
        });
      }
    }

    geocode.sort(sortByGeoCode);

    res.json({ total : geocode.length , geocode: geocode });
  } catch (error) {
    next(error);
  }
}

async function getAllGeocodeAmphoeWaterBalance(req, res, next) {
  try {
    const amphoe = ["wgo:wt-amphoe-phatthalung", "wgo:wt-amphoe_ratchaburi"];

    let geocode = [];
    for (let i = 0; i < amphoe.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${amphoe[i]}&maxFeatures=200&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      for (let j = 0; j < jsonData["features"].length; j++) {
        geocode.push({
          geoCode: jsonData["features"][j]["properties"]["AMP_CODE"],
          nameThai: jsonData["features"][j]["properties"]["DistrictNT"],
          nameEng: jsonData["features"][j]["properties"]["DistrictNE"],
        });
      }
    }

    geocode.sort(sortByGeoCode);

    res.json({ total : geocode.length , geocode: geocode });
  } catch (error) {
    next(error);
  }
}

async function getAllGeocodeProvinceWaterBalance(req, res, next) {
  try {
    const province = [
      "wgo:wt-province-phatthalung",
      "wgo:wt-province_ratchaburi",
    ];

    let geocode = [];
    for (let i = 0; i < province.length; i++) {
      const url = `${process.env.URL_WFS}/wgo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${province[i]}&maxFeatures=20&outputFormat=application%2Fjson`;
      const response = await axios.get(url);

      const jsonData = response.data;

      geocode.push({
        geoCode: jsonData["features"][0]["properties"]["PROV_CODE"],
        nameThai: jsonData["features"][0]["properties"]["PROV_NAM_T"],
        nameEng: jsonData["features"][0]["properties"]["PROV_NAM_E"],
      });
    }

    geocode.sort(sortByGeoCode);

    res.json({ total : geocode.length , geocode: geocode });
  } catch (error) {
    next(error);
  }
}
