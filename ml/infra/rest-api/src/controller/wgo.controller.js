require("dotenv").config();

const express = require("express");

const router = express.Router();

router.get("/", index);
router.get("/name/map", getNameMap);


router.post("/wgo/sensor", wgoInputSensor);

router.post("/hii/sensor", hiiInputSensor);

router.get("/wgo/predict", wgoPredict);

module.exports = router;


async function wgoPredict(req, res, next) {




  let  yesterday= getRandomDoubleWithDecimalPlaces(0 , 15.3 , 2)

  let  daily= getRandomDoubleWithDecimalPlaces(0 , 15.3 , 2)
  let  today= getRandomDoubleWithDecimalPlaces(0 , 15.3 , 2)
  let  tomorrow= getRandomDoubleWithDecimalPlaces(0 , 15.3 , 2)
  let  afterTomorrow= getRandomDoubleWithDecimalPlaces(0 , 15.3 , 2)

  let rateDailyChange = ((daily- yesterday) / yesterday) * 100

  let rateTodayChange = ((today- daily) / daily) * 100

  let rateTomorrowChange = ((tomorrow- today) / today) * 100

  let rateAfterTomorrowChange = ((afterTomorrow- tomorrow) / tomorrow) * 100

  let raw = 
  {
    "salinityYesterday" :yesterday,
    "salinityDaily" : daily,
    "rateDailyChange" : parseFloat(rateDailyChange.toFixed(2)),
    "salinityPredictToday" : today, 
    "rateTodayChange" : parseFloat(rateTodayChange.toFixed(2)),
    "salinityPredictTomorrow" : tomorrow, 
    "rateTomorrowChange" : parseFloat(rateTomorrowChange.toFixed(2)),
    "salinityPredictDayAfterTomorrow" : afterTomorrow,  
    "rateAfterTomorrowChange" : parseFloat(rateAfterTomorrowChange.toFixed(2))

  }

  res.json(raw)
}

async function hiiInputSensor(req, res, next) {



  res.status(201).json()
}

async function wgoInputSensor(req, res, next) {



  res.status(201).json()
}

async function getNameMap(req, res, next) {
  let names = [
    { name: "kaochaison", images: "http://127.0.0.1:3000/predict/01.png" },
    { name: "Ranod", images: "http://127.0.0.1:3000/predict/02.png" },
    { name: "Kasaesin", images: "http://127.0.0.1:3000/predict/03.png" },
  ];

  try {
    res.json(names);
  } catch (e) {
    next(e);
  }
}

async function index(req, res, next) {
  try {
    res.status(200).json({
      wgoVersion: process.env.WGO_VERSION,
      status: 200,
      result: Date.now(),
    });
  } catch (e) {
    next(e);
  }
}

function getRandomDoubleWithDecimalPlaces(min, max, decimalPlaces) {
  const randomDouble = Math.random() * (max - min) + min;
  return parseFloat(randomDouble.toFixed(decimalPlaces));
}
