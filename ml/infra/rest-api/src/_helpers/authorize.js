require('dotenv').config()

const jwt = require('express-jwt');
const pool = require('./database');



module.exports = authorizeUser;

function authorizeUser(roles = []) {



    
    return [
        jwt({ secret : process.env.JWT_ACCESS, algorithms: ['HS256'] }),
        async (req, res, next) => {

         

            try{

  
                const access_token = req.headers['authorization'].split(" ")[1];
                const refresh_token = req.headers['refresh-token'].split(" ")[1];
    
                var queryAccessToken = "select check_a_token('"+access_token+"') as 'total'";
                var resultAccessToken =  await pool.query(queryAccessToken);
    
                var queryRefreshToken = "select check_r_token('"+refresh_token+"') as 'total' ";
                var resultRefreshToken =   await pool.query(queryRefreshToken);
    
                if(resultAccessToken[0]['total'] == 0 || resultRefreshToken[0]['total'] == 0){
                    return res.status(401).json({ message: 'Unauthorized' });
                }
    
    
                if (roles.length && !roles.includes(req.user.type)) {
                    return res.status(401).json({ message: 'Unauthorized' });
                }
                next();
            }catch(e){
                next(e);
            }

            
        }
    ];
}