




function renameKey(obj, oldKey, newKey) {
  if (obj[oldKey]) {
    obj[newKey] = obj[oldKey];
    delete obj[oldKey];
  }
}

function sortByGeoCode(a, b) {
  return a.geoCode.localeCompare(b.geoCode);
}


const validateJSONKeys = (requiredKeys) => {
    return (req, res, next) => {
      const requestBody = req.body;
      const missingKeys = [];
  
      // Check if each required key is present in the request body
      requiredKeys.forEach((key) => {
        if (!(key in requestBody)) {
          missingKeys.push(key);
        }
      });
  
      // If any required key is missing, send an error response
      if (missingKeys.length > 0) {
        return res.status(400).json({ message: `Missing keys: ${missingKeys.join(', ')}` });
      }
  
      // All required keys are present, proceed to the next middleware
      next();
    };
  };


const validateJSONKeysAndTypes = (keyTypePairs) => {
  return (req, res, next) => {
    const requestBody = req.body;
    const errors = [];

    keyTypePairs.forEach(({ key, type }) => {
      if (!(key in requestBody)) {
        errors.push(`Missing key: ${key}`);
      } else if (requestBody[key] !== null && typeof requestBody[key] !== type) {
        errors.push(`Key '${key}' should be of type ${type}`);
      }
    });

    if (errors.length > 0) {
      return res.status(400).json({ message: errors.join(', ') });
    }

    next();
  };
};


  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
  

  module.exports = {
    validateJSONKeys,
    renameKey,
    getRandomInt,
    sortByGeoCode,
    validateJSONKeysAndTypes
  };
