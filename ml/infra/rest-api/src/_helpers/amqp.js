
require("dotenv").config();

var amqp = require("amqplib/callback_api");

const sendToQueue = (queue , msg) => {
  amqp.connect(
    process.env.CONNECTION_AMQP,
    function (error0, connection) {
      if (error0) {
        throw error0;
      }
      connection.createChannel(function (error1, channel) {
        if (error1) {
          throw error1;
        }
    
        channel.assertQueue(queue, {
          durable: false,
        });

        channel.sendToQueue(queue, Buffer.from(msg));
        setTimeout(function () {
          connection.close();
        }, 500);
      });
    }
  );
};

module.exports = {
  sendToQueue,
};
