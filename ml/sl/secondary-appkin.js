const express = require('express');
const app = express();
const port = 3001;

app.get('/', (req, res) => {
    res.send('Hello World! secondary-appkin');
});

app.get('/test', (req, res) => {
    res.send('path test secondary-appkin');
});

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}`);
});