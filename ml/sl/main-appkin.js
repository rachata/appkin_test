const express = require('express');
const app = express();
const port = 3000;

app.get('/', (req, res) => {
    res.send('Hello World! main-appkin');
});

app.get('/test', (req, res) => {
    res.send('path test main-appkin');
});

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}`);
});